use crate::StopWords;
use indexmap::IndexSet;
use std::collections::HashMap;
use std::io::Read;

pub struct Codex(IndexSet<String>);

pub struct Document(Vec<usize>);

pub struct LibraryBuilder {
    stopwords: Option<StopWords>,
    vocab: Option<IndexSet<String>>,
    raw_docs: Vec<String>,
    min_word_freq: u32,
    remove_punctuation: bool,
}

pub enum LibraryBuilderError {
    IoError(std::io::Error),
}

impl LibraryBuilder {
    pub fn from_raw(raw_docs: Vec<String>) -> Self {
        LibraryBuilder {
            stopwords: Some(StopWords::English),
            vocab: None,
            raw_docs,
            min_word_freq: 1,
            remove_punctuation: true,
        }
    }

    /// From a reader where each line is a document
    pub fn from_reader<R: Read>(
        reader: &mut R,
    ) -> Result<Self, std::io::Error> {
        let mut buf = String::new();
        reader.read_to_string(&mut buf)?;
        let raw_docs = buf.split('\n').map(String::from).collect();

        Ok(LibraryBuilder {
            stopwords: Some(StopWords::English),
            vocab: None,
            raw_docs,
            min_word_freq: 1,
            remove_punctuation: true,
        })
    }

    /// Set the vocabulary rather than derive one from the data.
    ///
    /// # Notes
    /// - Words not in the vocabulary will be discarded.
    /// - `min_word_freq` is still applied to user-defined vocabularies. If you
    ///   do not want a cutoff applied, set `min_word_freq` to 1.
    pub fn vocabulary<'a, T>(mut self, vocab: T) -> Self
    where
        T: IntoIterator,
        String: From<T::Item>,
    {
        let mut set: IndexSet<String> = IndexSet::new();
        vocab.into_iter().for_each(|s| {
            set.insert(String::from(s));
        });
        self.vocab = Some(set);
        self
    }

    /// If true, remove characters that are neither alphanumeric nor a space
    pub fn remove_punctuation(mut self, remove: bool) -> Self {
        self.remove_punctuation = remove;
        self
    }

    /// Set the minimum number of occurrences of a word to be included in the
    /// vocabulary
    ///
    /// # Notes
    /// `min_word_freq` is still applied to user-defined vocabularies. If you
    ///  do not want a cutoff applied, set `min_word_freq` to 1.
    pub fn min_word_freq(mut self, min_word_freq: u32) -> Self {
        self.min_word_freq = min_word_freq;
        self
    }

    /// Set the stop words
    pub fn stopwords(mut self, stopwords: StopWords) -> Self {
        self.stopwords = Some(stopwords);
        self
    }

    fn build_string_docs(
        &mut self,
    ) -> (HashMap<String, u32>, Vec<Vec<String>>) {
        let stopwords = self.stopwords.take().unwrap().to_set();

        // pass 1, remove stopwords
        let mut codex: IndexSet<String> = IndexSet::new();
        let mut counter: HashMap<String, u32> = HashMap::new();
        let remove_punctuation = self.remove_punctuation;
        let vocab = &self.vocab;

        let docs: Vec<Vec<String>> = self
            .raw_docs
            .iter()
            .map(|doc| {
                let words: Vec<String> = doc
                    .split(' ')
                    .filter_map(|s| {
                        if remove_punctuation {
                            let out: String = s
                                .chars()
                                .filter(|&c| c.is_alphabetic() || c == ' ')
                                .collect();
                            if out.is_empty() {
                                None
                            } else {
                                Some(out)
                            }
                        } else {
                            Some(String::from(s))
                        }
                    })
                    .map(|s| s.trim().to_lowercase())
                    .filter(|s| !stopwords.contains(s))
                    .filter(|s| {
                        if let Some(v) = vocab {
                            v.contains(s)
                        } else {
                            codex.insert(String::from(s));
                            true
                        }
                    })
                    .map(|s| {
                        // TODO: get rid of this clone
                        counter
                            .entry(s.clone())
                            .and_modify(|ct| *ct += 1)
                            .or_insert(1);
                        s
                    })
                    .collect();
                words
            })
            .collect();

        if let None = self.vocab {
            self.vocab = Some(codex)
        }

        (counter, docs)
    }

    /// Build the library and consume the builder.
    pub fn build(mut self) -> Library {
        let (counter, mut string_docs) = self.build_string_docs();

        // find words that do not occur enough and remove them from the codex/vocab
        self.vocab
            .take()
            .map(|mut codex| {
                counter.iter().for_each(|(key, &count)| {
                    if count < self.min_word_freq {
                        codex.remove(key);
                    }
                });

                let docs: Vec<Document> = string_docs
                    .iter_mut()
                    .map(|doc| {
                        let words: Vec<usize> = doc
                            .drain(..)
                            .filter_map(|word| {
                                codex.get_full(&word).map(|(ix, _)| ix)
                            })
                            .collect();
                        Document(words)
                    })
                    .collect();

                Library { docs, codex }
            })
            .expect("Failed to build codex")
    }
}

pub struct CondordanceEntry {
    /// The integer key of the word
    pub word: usize,
    /// The index of the word in the document
    pub word_ix: usize,
    /// The index of this word's document
    pub doc_ix: usize,
    /// The topic to which this word is assigned
    pub(crate) topic: usize,
}

pub struct Library {
    docs: Vec<Document>,
    codex: IndexSet<String>,
}

impl Library {
    #[inline(always)]
    pub fn docs(&self) -> &Vec<Document> {
        &self.docs
    }

    #[inline(always)]
    pub fn codex(&self) -> &IndexSet<String> {
        &self.codex
    }

    #[inline(always)]
    pub fn n_words(&self) -> usize {
        self.codex.len()
    }

    #[inline(always)]
    pub fn n_docs(&self) -> usize {
        self.docs.len()
    }

    pub fn to_concordance(&self) -> Vec<CondordanceEntry> {
        // get the total number of words in the library
        let n: usize = self.docs.iter().map(|doc| doc.0.len()).sum();

        let mut concordance: Vec<CondordanceEntry> = Vec::with_capacity(n);
        for (doc_ix, doc) in self.docs.iter().enumerate() {
            for (word_ix, &word) in doc.0.iter().enumerate() {
                let entry = CondordanceEntry {
                    word,
                    word_ix,
                    doc_ix,
                    topic: 0,
                };
                concordance.push(entry);
            }
        }
        concordance
    }
}
