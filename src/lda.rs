use rand::Rng;
use rv::dist::{Categorical, SymmetricDirichlet};
use rv::traits::Rv;

use crate::misc::ln_pflip;
use crate::CondordanceEntry;
use crate::Library;
use crate::Matrix;
use crate::TopicInfo;

#[derive(Clone, Copy, Debug)]
pub enum LdaAlgorithm {
    CollapsedGibbs,
}

impl Default for LdaAlgorithm {
    fn default() -> Self {
        LdaAlgorithm::CollapsedGibbs
    }
}

pub struct Lda<'a> {
    library: &'a Library,
    concordance: Vec<CondordanceEntry>,
    /// n_dt[(i, j)]  is the number of total occurrences of topic j in document i
    n_dt: Matrix<u32>,
    /// n_tw[(i, j)]  is the number of total occurrences of word j in topic i
    n_tw: Matrix<u32>,
    /// n_t[i]  is the number of total occurrences of topic i
    n_t: Vec<u32>,
    alpha: f64,
    beta: f64,
    /// Number of words in the vocabulary
    n_words: usize,
}

pub struct LdaArgs {
    pub alpha: f64,
    pub beta: f64,
    pub n_topics: usize,
}

impl Default for LdaArgs {
    fn default() -> Self {
        LdaArgs {
            alpha: 0.5,
            beta: 0.5,
            n_topics: 4,
        }
    }
}

impl<'a> Lda<'a> {
    pub fn init<R: Rng>(
        lib: &'a Library,
        args: LdaArgs,
        mut rng: &mut R,
    ) -> Self {
        let mut lda = Lda {
            library: lib,
            concordance: lib.to_concordance(),
            n_dt: Matrix::with_default(lib.n_docs(), args.n_topics),
            n_tw: Matrix::with_default(args.n_topics, lib.n_words()),
            n_t: vec![0; args.n_topics],
            alpha: args.alpha,
            beta: args.beta,
            n_words: lib.n_words(),
        };

        let dir_topic =
            SymmetricDirichlet::new(lda.alpha, args.n_topics).unwrap();
        let mut current_doc = 0;
        let weights: Vec<f64> = dir_topic.draw(&mut rng);
        let mut topic_dist = Categorical::new(&weights).unwrap();
        for entry in lda.concordance.iter_mut() {
            if entry.doc_ix != current_doc {
                let weights: Vec<f64> = dir_topic.draw(&mut rng);
                topic_dist = Categorical::new(&weights).unwrap();
                current_doc += 1;
            }

            entry.topic = topic_dist.draw(&mut rng);

            lda.n_dt[(entry.doc_ix, entry.topic)] += 1;
            lda.n_tw[(entry.topic, entry.word)] += 1;
            lda.n_t[entry.topic] += 1;
        }

        lda
    }

    #[inline(always)]
    pub fn n_words(&self) -> usize {
        self.n_words
    }

    #[inline(always)]
    pub fn n_docs(&self) -> usize {
        self.library.n_docs()
    }

    pub fn update<R: Rng>(
        &mut self,
        n_scans: usize,
        alg: LdaAlgorithm,
        mut rng: &mut R,
    ) {
        for _ in 0..n_scans {
            match alg {
                LdaAlgorithm::CollapsedGibbs => self.scan_gibbs(&mut rng),
            }
        }
    }

    fn scan_gibbs<R: Rng>(&mut self, mut rng: &mut R) {
        use rand::seq::SliceRandom;
        let ixs = {
            let mut ixs: Vec<usize> = (0..self.concordance.len()).collect();
            ixs.shuffle(&mut rng);
            ixs
        };
        for &entry_ix in ixs.iter() {
            self.step_gibbs(entry_ix, &mut rng)
        }
    }

    fn step_gibbs<R: Rng>(&mut self, entry_ix: usize, mut rng: &mut R) {
        let (doc_ix, word, topic) = {
            let entry = &self.concordance[entry_ix];
            (entry.doc_ix, entry.word, entry.topic)
        };

        // remove this entry from the counts
        self.n_dt[(doc_ix, topic)] -= 1;
        self.n_tw[(topic, word)] -= 1;
        self.n_t[topic] -= 1;

        let wbeta = self.beta * (self.n_words() as f64);

        let ln_conditionals: Vec<f64> = self
            .n_t
            .iter()
            .enumerate()
            .map(|(t, &ct)| {
                let a = (f64::from(self.n_dt[(doc_ix, t)]) + self.alpha).ln();
                // TODO: if we tend to access like this, we might want to put t
                // on the columns.
                let b = (f64::from(self.n_tw[(t, word)]) + self.beta).ln();
                let c = (f64::from(ct) + wbeta).ln();
                a + b - c
            })
            .collect();

        let topic = ln_pflip(ln_conditionals, &mut rng);

        self.concordance[entry_ix].topic = topic;

        self.n_dt[(doc_ix, topic)] += 1;
        self.n_tw[(topic, word)] += 1;
        self.n_t[topic] += 1;
    }

    pub fn topic_info(&self) -> TopicInfo {
        TopicInfo {
            word_list: self.library.codex().iter().cloned().collect(),
            n_tw: self.n_tw.clone(),
            n_t: self.n_t.clone(),
            top_n: 10,
        }
    }
}
