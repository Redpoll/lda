use crate::Matrix;

pub struct TopicInfo {
    pub(crate) word_list: Vec<String>,
    pub(crate) n_tw: Matrix<u32>,
    pub(crate) n_t: Vec<u32>,
    pub(crate) top_n: usize,
}

impl TopicInfo {
    pub fn top_n(mut self, n: usize) -> Self {
        self.top_n = n;
        self
    }

    pub fn display(&self) {
        let total_words: f64 = self.n_t.iter().sum::<u32>().into();
        let mut topic_props: Vec<(usize, u32)> = self
            .n_t
            .iter()
            .enumerate()
            .map(|(ix, &ct)| (ix, ct))
            .collect();

        topic_props.sort_by_key(|x| x.1);

        for (t, topic_count) in topic_props.iter().rev() {
            // let counts =
            // for (t, counts) in self.n_tw.rows().enumerate() {
            let mut distr: Vec<(usize, f64)> = (0..self.n_tw.ncols())
                .map(|w| {
                    let ct = self.n_tw[(*t, w)].clone();
                    let x = f64::from(ct);
                    (w, x)
                })
                .collect();

            distr.sort_unstable_by(|a, b| b.1.partial_cmp(&a.1).unwrap());
            // TODO: use pretty table tool
            println!(
                "\n Topic {}: {:1.3}%",
                t,
                f64::from(*topic_count) / total_words * 100.0
            );
            println!("---------------------");
            for i in 0..self.top_n {
                let word = &self.word_list[distr[i].0];
                let perc = distr[i].1 / total_words * 100.0;
                println!("{}: {:1.3}%", word, perc);
            }
        }
    }
}
