mod english;

use english::ENGLISH_STOPWORDS;
use std::collections::HashSet;

pub enum StopWords {
    English,
    Custom(HashSet<String>),
}

impl StopWords {
    pub fn union(&self, other: &StopWords) -> StopWords {
        unimplemented!()
    }

    pub fn to_set(self) -> HashSet<String> {
        match self {
            Self::Custom(set) => set,
            Self::English => {
                ENGLISH_STOPWORDS.iter().map(|&s| String::from(s)).collect()
            }
        }
    }
}

impl<T: Into<String>> From<Vec<T>> for StopWords {
    fn from(mut words: Vec<T>) -> StopWords {
        let set: HashSet<String> =
            words.drain(..).map(|s| s.into().to_lowercase()).collect();
        StopWords::Custom(set)
    }
}
