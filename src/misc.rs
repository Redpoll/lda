use rand::Rng;

pub fn ln_pflip<R: Rng>(mut ln_ps: Vec<f64>, rng: &mut R) -> usize {
    let maxval = {
        let first = ln_ps[0];
        ln_ps.iter().skip(1).fold(
            first,
            |max, &p| {
                if p > max {
                    p
                } else {
                    max
                }
            },
        )
    };

    let mut last_p = (ln_ps[0] - maxval).exp();
    ln_ps[0] = last_p;
    ln_ps.iter_mut().skip(1).for_each(|p| {
        *p = (*p - maxval).exp() + last_p;
        last_p = *p;
    });

    let last: f64 = ln_ps.last().unwrap().to_owned();
    let u = rng.gen::<f64>() * last;

    for (i, &p) in ln_ps.iter().enumerate() {
        if p > u {
            return i;
        }
    }
    unreachable!();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ln_pflip_smoke() {
        let mut rng = rand::thread_rng();

        let mut bins = vec![0.0_f64; 10];
        let n = 100_000;
        let incr = f64::from(n).recip();

        for _ in 0..n {
            let ln_ps = vec![0.0_f64; 10];
            let ix = ln_pflip(ln_ps, &mut rng);

            assert!(ix < 10);

            bins[ix] += incr;
        }

        if bins.iter().any(|w| (w - 0.1).abs() > 0.01) {
            panic!("Bad Bins: {:?}", bins);
        }
    }
}
