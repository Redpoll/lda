use lda::{Lda, LdaAlgorithm, LdaArgs, LibraryBuilder};
use std::error::Error;
use std::fs::File;
use std::path::Path;
use std::process;

fn example() -> Result<(), Box<dyn Error>> {
    // Path to the input file. Each line in the file is a document.
    let path = Path::new("resources").join("imdb.txt");
    let mut f = File::open(path)?;

    // Generate the library. Remove punctuation, lower-case all the words,
    // remove words that occur fewer than 10 times.
    let library = LibraryBuilder::from_reader(&mut f)?
        .remove_punctuation(true)
        .min_word_freq(10)
        .build();

    // Assume 16 topics. Use default alpha and beta params.
    let args = LdaArgs {
        n_topics: 16,
        ..Default::default()
    };

    let mut rng = rand::thread_rng();

    // Initialize the LDA from the prior then run the collapsed Gibbs algorithm
    // for 1000 steps.
    let mut lda = Lda::init(&library, args, &mut rng);
    lda.update(1_000, LdaAlgorithm::CollapsedGibbs, &mut rng);

    // show top 15 words for each topic
    lda.topic_info().top_n(15).display();

    Ok(())
}

fn main() {
    if let Err(err) = example() {
        println!("error running example: {}", err);
        process::exit(1);
    }
}
